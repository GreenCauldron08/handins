import Club, { Member, Gender } from './Club';
import { AbstractClub } from './Part1_Inheritance';

/* A02
	Implement the following classes according to their documention:
		GeneralClub, ClubFactory
		ClubFactory should be the default export
		GeneralClub should be exported as GeneralClub.
	You may not use any of the code from Assignment 1 except where noted.

	You should only need 1 linter exception; for a necessary empty private constructor
	// eslint-disable-next-line @typescript-eslint/no-empty-function
*/

/**
	A general purpose club that uses *dependency injection* to determine if a given member may join the club.
	@remarks
	`GeneralClub` is-a `AbstractClub` from Assignment 1.
	You may not alter the public interface from `Club`.
	The constructor accepts (only) a predicate of the following type:
		`(m: Member, c: Club) => boolean`
		`m` - the member who wishes to join the club
		`c` - the club the member wishes to join. ie: this club
	You must implement the `canAdd` method to use the given predicate.
	You may not subclass this class.
	This class should be exported as `GeneralClub`.
*/
// TODO: create GeneralClub class here
class GeneralClub extends AbstractClub{
	constructor(private readonly predicate:(m:Member,c:Club)=>boolean){
        super();
    }
    canAdd(m:Member):boolean{
    	return this.predicate(m,this)&&(!this.contains(m));
    }
}
exports.GeneralClub=GeneralClub;
/** A factory for making clubs!
	@remarks
	`ClubFactory`
	This factory creates instances of `GeneralClub` by assigning an appropriate predicate.
	You may not modify a created `GeneralClub` in any way -- only by providing the necessary predicate.
	This is a factory class, so it should have no instances.
		Make the constructor private (and empty).
			You will have to disable the linter's error for that line.
		Determine the correct access modifier for its methods.
	This class should be the default export for this module.
	You may not use any of the clubs from Assignment 1.
*/
// TODO: create ClubFactory utility class here
export default class ClubFactory{
	//eslint-disable-next-line @typescript-eslint/no-empty-function
	private constructor(){}

	// constructor()

	/** Creates a club which only allows members who are
		18 years old or older.
		`adultsOnlyClub`
	*/
	// adultsOnlyClub
	static adultsOnlyClub():Club{
		return new GeneralClub(function(m:Member,c:Club){
			return m.age>=18;
		});
	}

	/** Creates a club which only allows members who are
		Female and single.
		`allTheSingleLadiesClub`
	*/
	// allTheSingleLadiesClub
	static allTheSingleLadiesClub():Club{
		return new GeneralClub(function(m:Member,c:Club){
			return m.isSingle;
		});
	}

	/** Creates a club which only allows
		the given maximum number of members.
		`sizeLimitClub`
		@param limit - the maximum number of club members
	*/
	// sizeLimitClub(limit: number)
	static sizeLimitClub(limit:number):Club{
		return new GeneralClub(function(m:Member,c:Club){
			return c.size<limit;
		});
	}

	/** Creates a club which does not allow anyone whose
		name begins with "Homer" to join the club.
		`noHomerClub`
	*/
	// noHomerClub()
	static noHomerClub():Club{
		return new GeneralClub(function(m:Member,c:Club){
			return !m.name.startsWith("Homer");
		});
	}

	/** Creates a club which allows at most one of it's member's
		names to begin with "Homer".
		ie: one nember may be named "Homer", but a second "Homer" is not allowed to join.
		`noHomersClub`
	*/
	// noHomersClub()
	static noHomersClub():Club{
		return new GeneralClub(function(m:Member,c:Club){
			return (!m.name.startsWith("Homer"))||c.count(function(theM:Member){
				return theM.name.startsWith("Homer");
			})<2;
		});
	}

	/** Creates a club which keeps the number of male and female members within 1 of each other.
		ie: there can never be more than 1 male more than the number of females, and vice versa.
		Do not try to reorder applicants. Apllicants are accepted or rejected in the order which they apply.
		Do not try to track state in the predicate.  Use the club's public accessors.
		`balancedGendersClub`
	*/
	// balancedGendersClub()
	static balancedGendersClub():Club{
		return new GeneralClub(function(m:Member,c:Club){
			return Math.abs(c.count(function(theM:Member){return theM.gender==Gender.M})-c.count(function(theM:Member){return theM.gender==Gender.F})+m.gender)<=1;
		});
	}
}
