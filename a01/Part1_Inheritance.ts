//eslint-disable-next-line @typescript-eslint/no-unused-vars
import Club, { Member, Gender } from './Club';

/* A01
	Implement the following classes in this file according to their documention:
		AbstractClub, LimitedSizeClub, NoHomerClub, BalancedGendersClub

	You may not use any of the code from Assignment 2.
	All classes in this module should be exported, and none of them should be a default export.

	You should only need 2 linter exceptions:
		*  for an explicit any when required for overloading
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
		*  for an unused variable that was specified by the interface, but not used by the contract
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
*/

/** An abstract club missing only the `canAdd` method.
	Subclasses should override `canAdd`.
	@remarks
	`AbstractClub` is-a `Club`
	The class's constructor (if any) does not accept any parameters.
	The list of members must be kept `private`.
	You may not alter the public interface from `Club`.
	You may not implement the `canAdd` method -- leave it abstract.
	You must *overload* the `contains` method.
	No subclasses may maintain their own list of members.
*/
// TODO:  create AbstractClub class here
export abstract class AbstractClub implements Club{
	private members:Member[]=[];
	size:number=0;
	add(...m:Member[]):boolean{
		let change:boolean=false;
		for(let mem of m){
			if(this.canAdd(mem)&&(!this.contains(mem))){
				this.members.push(mem);
				this.size++;
				change=true;
			}
		}
		return change;
	}
	contains(m:Member):boolean{
		for(let mem of this.members)
			if(mem==m)
				return true;
		return false;
		//return this.contains((mem:Member)=>{return mem==m;});
	}
	contains(predicate:(m:Member)=>boolean):boolean{
		for(let mem of this.members)
			if(predicate(mem))
				return true;
		return false;
	}
	count(predicate:(m:Member)=>boolean):number{
		let c:number=0;
		for(let mem of this.members)
			if(predicate(mem))
				c++;
		return c;
	}
	abstract canAdd(m:Member):boolean;
};
// *** CLUBS ***

/** A club limited to a certain number of members.
	Once this number is reached, no other members are permitted to join.
	@remarks
	`LimitedSizeClub` is-a `AbstractClub`
	The class's constructor accepts a `number` that is the maximum number of club members.
	The `canAdd` method should optionally take a `Member` as its parameter.
*/
// TODO:  create LimitedSizeClub class here
export class LimitedSizeClub extends AbstractClub{
	constructor(private readonly maxMems:number){
		super();
	}
	canAdd(m:Member):boolean{
		return this.size<this.maxMems&&(!this.contains(m));
	}
}
/** A club that does not allow anyone whos name begins with "Homer" to join the club.
	@remarks
	`NoHomerClub` is-a `AbstractClub`
	The class's constructor (if any) does not accept any parameters.
*/
// TODO:  create NoHomerClub class here
export class NoHomerClub extends AbstractClub{
	canAdd(m:Member):boolean{
		return !(m.name.startsWith("Homer")||this.contains(m));
	}
}

/** A club which keeps the number of male and female members within 1 of each other.
	ie: there can never be more than 1 male more than the number of females, and vice versa.
	@remarks
	`BalancedGendersClub` is-a `AbstractClub`
	The class's constructor (if any) does not accept any parameters.
	Override the `add` method to keep track of the number of men vs number of women in the club.
	Do not try to re-order members that wish to join. They are accepted or rejected in the order in which they apply.
*/
// TODO:  create BalancedGendersClub class here
export class BalancedGendersClub extends AbstractClub{
	private balance:number=0;
	canAdd(m:Member):boolean{
		return (Math.abs(this.count(function(theM:Member){return theM.gender==Gender.M})-this.count(function(theM:Member){return theM.gender==Gender.F})+m.gender)<=1)&&(!this.contains(m));
	}
}